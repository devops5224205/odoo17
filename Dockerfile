# Use the official Odoo image from Docker Hub
FROM odoo:16

# Set the working directory in the container
WORKDIR /usr/src/odoo

# Copy the custom addons into the container
COPY ./backend_addons /mnt/extra-addons
COPY ./odoo.conf /etc/odoo/odoo.conf
COPY install_modules.sh /install_modules.sh
COPY modules.txt /modules.txt

USER root
RUN chmod -R 777 /modules.txt
RUN chmod -R 777 /install_modules.sh
RUN chmod -R 777 /mnt/extra-addons
RUN chmod -R 777 /etc/odoo/odoo.conf
RUN chown -R odoo:odoo /mnt/extra-addons
RUN chown -R odoo:odoo /etc/odoo/odoo.conf
RUN chown -R odoo:odoo /install_modules.sh
RUN chown -R odoo:odoo /modules.txt
RUN apt-get update && apt-get install -y procps && rm -rf /var/lib/apt/lists/*

# Expose the Odoo ports (you might need to expose additional ports depending on your setup)
EXPOSE 8069 8071
USER odoo
# Start Odoo when the container launches
CMD ["odoo"]
